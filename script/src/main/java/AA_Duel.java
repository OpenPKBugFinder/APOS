import com.aposbot.Constants;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Endlessly duels in Lumbridge
 * <p>
 * Required:
 * Start script at the Lumbridge spawn with sleeping bag in inventory.
 * <p>
 * Parameters:
 * -f,--fightmode <controlled|attack|strength|defense> (default strength)
 * -p,--partner (Person you are duelling)
 * <p>
 *
 * @Author Cody
 */
public class AA_Duel extends AA_Script {

	private static final long MAX_TRADE_DURATION = 1000L;

	private String duelPartner = "";
	private long startTime;

	private long duelTimeout;
	private long duelRequestTimeout;
	private long duelOfferTimeout;

	private int prevYCoord;

	private boolean duelAcceptSent;
	private boolean duelConfirmSent;
	private boolean initialized;

	public AA_Duel(final Extension ex) {
		super(ex);
	}

	@Override
	public void init(final String parameters) {
		if (!bot.isLoggedIn()) {
			throw new IllegalStateException("Must be logged-in to start this script.");
		}		
		if (!parameters.isEmpty()) {
			List<Integer> ids = null;

			final String[] args = parameters.split(" ");

			for (int i = 0; i < args.length; i++) {
				switch (args[i].toLowerCase()) {
					case "-p":
					case "--partner":
						this.duelPartner = args[++i].replace('_', ' ');
						break;
					case "-f":
					case "--fightmode":
						this.combatStyle = CombatStyle.valueOf(args[++i].toUpperCase());
						break;						
					default:
						throw new IllegalArgumentException("Error: malformed parameters. Try again ...");
				}
			}

		}
	}		
		

	@Override
	public int main() {
		setCombatStyle(combatStyle.getIndex());

		if (getFatigue() >= 95) {
			return sleep();
		}		
		
		return duel();
	}

	@Override
	public void onServerMessage(final String message) {
		if (message.startsWith("declined", 17)) {
			reset();
		} else if (message.startsWith("Unable")) {
			stopScript();
			System.err.println("Error: Could not PM duel partner.");
		} else if (message.contains("granted")) {
			reset();
		} else if (message.contains("defeated")) {
			reset();
		}
	}

	private void reset() {
		duelTimeout = 0L;
		duelRequestTimeout = 0L;
		duelOfferTimeout = 0L;


		bot.setInDuelOffer(false);
		setDuelAccepted(false);

		bot.setInDuelConfirm(false);
		setDuelConfirmed(false);

		duelConfirmSent = false;
		duelAcceptSent = false;
	}

	private void setDuelAccepted(final boolean accepted) {
		bot.Pj = accepted;
	}

	private void setDuelConfirmed(final boolean confirmed) {
		bot.dd = confirmed;
	}


	private String getPlayerName(final Object player) {
		final String name = ((ta) player).c;

		if (name == null) {
			return null;
		}

		return name.replace((char) 160, ' ');
	}


	public void onDuelRequest(final String playerName) {
		if (duelPartner.isEmpty()) {
			duelPartner = playerName;
			System.out.println("- Duel Partner: " + duelPartner);
		}

		if (!playerName.equalsIgnoreCase(duelPartner) || isPlayerBusy()) {
			return;
		}

		final int playerIndex = getPlayerIndexByName(duelPartner);

		if (playerIndex == -1) {
			return;
		}

		sendRequestDuel(playerIndex);
	}


	public void sendDuelRequest(final int serverIndex) {
		bot.createPacket(Constants.OP_PLAYER_DUEL);
		bot.put2(serverIndex);
		bot.finishPacket();
	}

	public void acceptDuel() {
		bot.createPacket(Constants.OP_DUEL_ACCEPT);
		bot.finishPacket();
		setDuelAccepted(true);
	}

	public void confirmDuel() {
		bot.createPacket(Constants.OP_DUEL_CONFIRM);
		bot.finishPacket();
		setDuelConfirmed(true);
	}

	public void declineDuel() {
		bot.createPacket(Constants.OP_DUEL_DECLINED);
		bot.finishPacket();
		bot.setInDuelOffer(false);
		bot.setInDuelConfirm(false);
		setDuelAccepted(false);
		setDuelConfirmed(false);
	}

	private boolean isPlayerBusy() {
		return isDuelOpen();
	}

	private int getPlayerIndexByName(final String name) {
		for (int index = 0; index < bot.getPlayerCount(); index++) {
			final Object player = bot.getPlayer(index);

			if (player == null) {
				continue;
			}

			final String playerName = getPlayerName(player);

			if (name.equalsIgnoreCase(playerName)) {
				return index;
			}
		}

		return -1;
	}

	private int sendRequestDuel(final int playerIndex) {
		if (System.currentTimeMillis() <= duelRequestTimeout) {
			return 0;
		}

		sendDuelRequest(getPlayerPID(playerIndex));
		duelRequestTimeout = System.currentTimeMillis() + 2000L;
		duelTimeout = System.currentTimeMillis() + MAX_TRADE_DURATION;
		return 0;
	}


	private int duel() {
		final int playerIndex = getPlayerIndexByName(duelPartner);

		if (playerIndex == -1) {
			if (bot.isInDuelOffer()) {
				declineDuel();
				reset();
			}

			return 0;
		}

		if (bot.isInDuelConfirm()) {
			return sendConfirmDuel();
		}

		if (bot.isInDuelOffer()) {

			return sendAcceptDuel();
		}


		return sendRequestDuel(playerIndex);
	}

	private int sendAcceptDuel() {
		if (!duelAcceptSent && !bot.hasLocalAcceptedTrade()) {
			if (bot.hasRemoteAcceptedDuel()) {
				acceptDuel();
				bot.displayMessage("@or1@Duel accepted.");
				duelAcceptSent = true;
			}
		}
				acceptDuel();
				duelAcceptSent = true;				
		return 0;
	}

	private int sendConfirmDuel() {
		if (!duelConfirmSent && !bot.hasLocalConfirmedDuel()) {
			confirmDuel();
			bot.displayMessage("@or2@Duel confirmed.");
			duelConfirmSent = true;
		}
			confirmDuel();
			duelConfirmSent = true;			
		return 0;
	}



	}
